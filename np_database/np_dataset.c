//SYSTEM
#include <stdlib.h>
//PROJECT
#include "../lib/sds.h"
#include "../lib/dict.h"
#include "../np_datastructure/np_dictionary.h"
#include "np_dataset.h"
//---------------------------------------------------------------------------------------------------------------------

np_db_dataset_field_t *np_db_dataset_field_init(void){
    np_db_dataset_field_t *dict_field;
    dict_field = malloc(sizeof(np_db_dataset_field_t));
    if (!dict_field)
        return NULL;
    return dict_field;
}

void np_db_dataset_add_field(np_db_dataset_field_t *dict_field){

    dict *column;
    column=np_ds_dict_init();
    dictAdd(column,"column_name",dict_field->column_name);
    dictAdd(column,"column_data_type",dict_field->column_data_type);
    dictAdd(column,"column_name_father",dict_field->column_name_father);

    dictAdd(dict_field->row,column,dict_field->value);
}