//SYSTEM
#include <mongoc.h>
//PROJECT
#include "../lib/sds.h"
#include "../lib/dict.h"
#include "np_datasource.h"
#include "np_mongo.h"
//---------------------------------------------------------------------------------------------------------------------
//FUNCTIONS
void np_db_configure_datasource_mongo(np_datasource_t *np_datasource) {
    char *uri;
    mongoc_client_t *mongoc_client;
    mongoc_database_t *mongoc_database;

    //configuration datasource
    uri = sdsnew("mongodb://");
    uri = sdscat(uri, np_datasource->database_user_name);
    uri = sdscat(uri, ":");
    uri = sdscat(uri, np_datasource->database_user_password);
    uri = sdscat(uri, "@");
    uri = sdscat(uri, np_datasource->database_host);
    uri = sdscat(uri, ":");
    uri = sdscat(uri, np_datasource->database_port);

    mongoc_init();

    //load connection
    mongoc_client = mongoc_client_new(uri);
    mongoc_client_set_appname(mongoc_client, "np_app");

    //load np_database
    mongoc_database=mongoc_client_get_database(mongoc_client,np_datasource->database_name);

    np_datasource->database_connection = mongoc_client;
    np_datasource->database=mongoc_database;
}
void np_db_datasource_free_mongo(np_datasource_t *np_datasource){
    mongoc_client_destroy((mongoc_client_t *)np_datasource->database_connection);
    mongoc_cleanup();
}
void np_db_repository_insert_mongodb(np_datasource_t *np_datasource,char *dataset_name, dict *data) {
    mongoc_database_t *mongoc_database;
    mongoc_collection_t *mongoc_collection;

    bson_t *bson;
    bson_oid_t oid;
    bson_error_t error;


    mongoc_database=(mongoc_database_t *) np_datasource->database;
    mongoc_collection = mongoc_database_get_collection(mongoc_database, dataset_name);

    dictIterator* di;
    dictEntry* de;
    di=dictGetIterator(data);
    while((de=dictNext(di))!=NULL){
        //printf(" {%s:%s} \n",de->key,dictGetVal(de));
    }

    bson = bson_new();
    bson_oid_init(&oid, NULL);
    BSON_APPEND_OID(bson, "_id", &oid);
    BSON_APPEND_UTF8(bson, "alguino2", "otro algo2");
    if (!mongoc_collection_insert_one(mongoc_collection, bson, NULL, NULL, &error)) {
        fprintf(stderr, "%s\n", &error.message);
    }
    bson_destroy(bson);

    mongoc_collection_destroy(mongoc_collection);

}
//---------------------------------------------------------------------------------------------------------------------