#include <stdlib.h>

#include "lib/sds.h"
#include "lib/dict.h"

#include "np_database/np_datasource.h"
#include "np_database/np_database.h"
#include "np_database/np_dataset.h"
#include "np_datastructure/np_dictionary.h"
#include "np_time/np_time.h"
#include "np_database/np_mongo.h"

int main() {

    np_datasource_t *np_datasource;
    np_datasource = np_db_datasource_init();

    np_datasource->database_host = sdsnew("127.0.0.1");
    np_datasource->database_port = sdsnew("27017");
    np_datasource->database_name = sdsnew("database_name");
    np_datasource->database_user_name = sdsnew("rodrigo");
    np_datasource->database_user_password = sdsnew("rodrigo");

    dict *row1;
    row1=np_ds_dict_init();

    np_db_dataset_field_t *dict_name;
    dict_name=np_db_dataset_field_init();
    dict_name->row=row1;
    dict_name->column_name=sdsnew("name");
    dict_name->column_data_type=sdsnew("string");
    dict_name->column_name_father=sdsnew("null");
    dict_name->value=sdsnew("rodrigo");
    np_db_dataset_add_field(dict_name);

    np_db_configure_datasource(np_datasource);
    np_db_repository_insert_mongodb(np_datasource,sdsnew("collection01"), row1);

    np_db_datasource_free(np_datasource);

    np_tm_print_current_time();

    return EXIT_SUCCESS;
}