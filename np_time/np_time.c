//SYSTEM
#include <time.h>
#include <stdio.h>
//PROJECT
#include "np_time.h"
//---------------------------------------------------------------------------------------------------------------------
//FUNCTIONS
void np_tm_print_current_time(void){
    struct timespec ts;
    struct tm* t;
    char buff[26];

    timespec_get(&ts, TIME_UTC);
    t = localtime(&ts.tv_sec);

    strftime(buff, 26, "%Y:%m:%d %H:%M:%S", t);
    printf("[%s.%ld|T:53158]\n", buff,ts.tv_nsec);
}